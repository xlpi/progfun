import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}
import scala.util.Random

/**
  * A Scala 'Future' example from the Scala Cookbook.
  * @see http://shop.oreilly.com/product/0636920026914.do
  */
object FuturesExample3 extends App {

  // not too exciting, the result will always be 42. but more importantly, when?
  println("1 - starting calculation ...")
  val f = Future {
    sleep(Random.nextInt(2500))
    42
  }

  println("2- before onComplete")
  f.onComplete {
    case Success(value) => {println(s"Got the callback, meaning = $value"); value }
    case Failure(e) => e.printStackTrace
  }

  // do the rest of your work
  println("A ..."); sleep(500)
  println("B ..."); sleep(500)
  println("C ..."); sleep(500)
  println("D ..."); sleep(500)
  println("E ..."); sleep(500)
  println("F ..."); sleep(500)


  // keep the jvm alive (may be needed depending on how you run the example)
  //sleep(2000)

  def sleep(duration: Long) { Thread.sleep(duration) }

}