object primes {
  def from(n: Int): Stream[Int] = {
   // println("stage " + n)
    n #:: from(n + 1)}

  from(100)  // eval only once for 100

  val nats = from(0)
  val multipleBy4 = nats map (_ * 4)

  val first100mBy4 = (multipleBy4 take 10)

  first100mBy4.toList

// the Sieve Eratosthenes - technique of calc primes

  def sieve(s: Stream[Int]): Stream[Int] =
    s.head #:: sieve(s.tail filter ( _ % s.head != 0))

val primes = sieve(from(2))

  primes.indexOf(7)

  val first100Primes = primes.take(10).toList

def sqrtStream(x: Double): Stream[Double] = {
  def improve(guess: Double) = (guess + x / guess) / 2
  lazy val guesses: Stream[Double] = 1 #:: (guesses map improve)
  guesses
}

  def isGoodEnough(guess: Double, x: Double) = math.abs((guess * guess - x) / x) < 0.0001

sqrtStream(4.0).take(10).toList

  sqrtStream(4.0).filter (isGoodEnough(_, 4)).take(10).toList






}