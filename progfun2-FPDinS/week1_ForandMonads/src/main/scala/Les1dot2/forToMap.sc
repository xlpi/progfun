// TODO: translate

case class Book(title: String, authors: List[String])

val books = List(
  Book("Structure and Interpretation of Computer Programs", List("Abelson, Harald", "Sussman, Gerald J.")),
  Book("Introduction to Functional Programming",
    List("Bird, Richard", "Wadler, Phil")),
  Book("Effective Java", List("Bloch, Joshua")),
  Book("Java Puzzlers", List("Bloch, Joshua", "Gafter, Neal")),
  Book("Programming in Scala", List(
    "Odersky, Martin", "Spoon, Lex", "Venners, Bill"
  ))
)

val forRes = {
  for( b <- books;
       a <- b.authors
    if a startsWith "Bird") yield b.title
}

val mapRes =
  books flatMap ( b =>
    b.authors withFilter ( a => a startsWith "Bird") map ( y => b.title))

books map (x => x.title + " -->")
books flatMap (x => x.title + " -->")
val mapFromFlat = books flatMap (x => List(x.title))