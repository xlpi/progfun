import java.util.Properties

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

trait Generator[+T] {
  self => //an alias for "this"

  def generate: T

  def map[S](f: T => S): Generator[S] = new Generator[S] {
    def generate = f(self.generate)
  }

  def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
    def generate = f(self.generate).generate
  }

  // SOME OTHER BUILDINGS Blocks
  def single[T](x: T):Generator[T] = new Generator[T] {
    def generate: T = x
  }

  def choose(lo: Int, hi: Int): Generator[Int] =
    for(x <- generators.integers) yield lo + x % (hi - lo)


//  def oneOf[T](xs: T*): Generator[T] =
//    for(idx <- choose(0,xs.length)) yield xs

  def lists: Generator[List[Int]] = for {
    isEmpty <- generators.booleans
    list <- if (isEmpty) emptyLists else nonEmptyLists
  } yield list

  def emptyLists = single(Nil)

  def nonEmptyLists = for {
    head <- generators.integers
    tail <- lists
  } yield head :: tail

}

trait Tree

case class Inner(left: Tree, right: Tree) extends Tree
case class Leaf(x: Int) extends Tree

// hint: a tree is either a leaf or an inner node



object generators extends App with Properties("String") {

  // some instance

  val integers = new Generator[Int] {
    val rand = new java.util.Random()
    def generate = rand.nextInt()
  }

  val booleans0 = new Generator[Boolean] {
    def generate = integers.generate > 0
  }
  // expanded to
  val booleans1 = integers map ( x => x > 0)

  val pairs = new Generator[(Int, Int)] {
    def generate: (Int, Int) = (integers.generate, integers. generate)
  }



  //ideal instancieted by for
//  val booleans2 = for( x<- integers) yield x > 0

  def pairs[T, U](t: Generator[T], u: Generator[U]) = for {
    x <- t
    y <- u
  } yield (x, y) // compiled

//  def pairsWithMap[T, U](t: Generator[T], u: Generator[U] = t flatMap ( x => u map ( y => (x, y))))
/*
Needed map and flatMap for that!
 */

  def lists: Generator[List[Int]] = for {
    isEmpty <- booleans
    list <- if (isEmpty) emptyLists else nonEmptyLists
  } yield list


  def emptyLists = single(Nil)

  def nonEmptyLists = for {
    head <- integers
    tail <- lists
  } yield head :: tail
  // booleans can write with For


  val booleans3 = for(x <- integers) yield x > 0




 // test use booleans3.foreach(println( _ ))

  val booleans31 = integers map ( x => x > 0)
//  val boolean32 = new Generator[Boolean] {
//    def generate = (x: Int => x > 0)(integers.generate)
//  }

  val booleans33 = integers.generate > 0

 // val someColor = oneOf("red","blue", "yellow")


// LETS GO TO WORK SHEET

//  val integers = new Generator[Int] {
//    val rand = new java.util.Random()
//    def generate = rand.nextInt()
//  }

  val booleans = integers map (_ >= 0)

  def leafs: Generator[Leaf] = for {
    x <- integers
  } yield Leaf(x)

  def inners: Generator[Inner] = for {
    l <- trees
    r <- trees
  } yield Inner(l, r)

  def trees: Generator[Tree] = for {
    isLeaf <- booleans
    tree <- if(isLeaf) leafs else inners
  } yield tree

  def single[T](x: T):Generator[T] = new Generator[T] {
    def generate: T = x
  }



  //   USE Tree Generator

  println(trees.generate)

  // Random Test Function

  def test[T](g: Generator[T], numTimes: Int = 100)
             (test: T => Boolean): Unit = {
    for(_ <- 0 until numTimes) {
      val value = g.generate
      assert(test(value), "test failed for " + value)
    }
    println("passed " + numTimes + " tests")
  }

  test(pairs(lists, lists)) {
    case(xs, ys) => (xs ++ ys).length > xs.length
  } // no allow all test ? dont work


  property("concatenate") = forAll {
    (l1: List[Int], l2: List[Int]) =>
      l1.size + l2.size == (l1 + l2).size
  }



  //some play
  //val genedInt = integers.generate // one item

// experiment how often random -100 to 100
//  val hiStream = Stream(1000000000)
//
//  val genedInts = hiStream map (_ => integers.generate) filter(_ < 100000)
//
//
//  println("1 to " + hiStream.length + " random which -100 to 100")
//  for(i <- 0 until genedInts.length
//      ) { println(i + "-nd item = " + genedInts(i))}



}
