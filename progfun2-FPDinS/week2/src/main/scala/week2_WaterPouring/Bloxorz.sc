// implementterrainFunction

val level =
        """ST
          |oo
          |oo"""

/**val level =
       """ST
         |oo
         |oo""".stripMargin

 is represented as
   Vector(Vector('S', 'T'), Vector('o', 'o'), Vector('o', 'o'))
  */


val toVVChar = {
  for(line <- {
    for(v <- level.split("\n")) yield v.stripMargin
  }.toList) yield line.toList.toVector
}.toVector

 private lazy val vector: Vector[Vector[Char]] = Vector(level.split("\n").map(str => Vector(str: _*)): _*)

//val charPoint = toVVChar.indexWhere(x => if(x.indexWhere(y => if(y == 'T') true else false))

case class Pos(row: Int, col: Int) {
  /** The position obtained by changing the `row` coordinate by `d` */
  def deltaRow(d: Int): Pos = copy(row = row + d)

  /** The position obtained by changing the `col` coordinate by `d` */
  def deltaCol(d: Int): Pos = copy(col = col + d)
}

case class Block(b1: Pos, b2: Pos) {
  def isStanding: Boolean = {
    if((b1.row == b2.row) && (b1.col == b2.col)) true else false
  }
}



val newBlock = Block(Pos(1,0), Pos(1,1))

newBlock.isStanding

if (false || false) false else true
(false && false)


