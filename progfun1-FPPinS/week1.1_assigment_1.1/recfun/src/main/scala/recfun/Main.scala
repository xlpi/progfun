package recfun

import scala.annotation.tailrec

object Main {

  def main(args: Array[String]) {

    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || r == 0) 1
    else if (r == 1) c
    else {
      fact(r) / (fact(c) * fact(abs(r - c)))
    }
  }

  def fact(n: Int) = {
    @tailrec
    def loop(c: Int, acc: Int): Int = {
      if (c <= 1) acc
      else loop(c - 1, c * acc)
    }
    loop(n, 1)
  }

  def abs(x: Int): Int = if (x < 0) -x else x

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {

    def closeO(myList: List[String]): List[String] = {
      val index = myList.indexOf("o")
      if (index == -1) myList
      else myList.updated(index, "c")
    }

    {
      def balanceCount(tail: List[Char]): Boolean = {

        def inLoop(tail: List[Char], acc: List[String]): List[String] = {

          if (tail.length == 0) acc
          else inLoop(tail.tail, {
            if (tail.head == '(') acc :+ "o"
            else if (tail.head == ')') {
              if (acc.contains("o")) closeO(acc)
              else acc :+ "o"
            } else acc
          })
        }

        if (!(inLoop(tail, List[String]()).contains("o"))) true else false
      }

      if (chars.isEmpty) throw (new NoSuchElementException)
      else balanceCount(chars)
    }

  }

  /**
    * Exercise 3
    */

  def countChange(money: Int, coins: List[Int]): Int = {
    def inLoop(amount: Int, list: List[Int]): Int = {
      val cond = {
        list.isEmpty || (amount < 0)
      }
      if (amount == 0) 1
      else if (cond) 0
      else inLoop(amount, list.tail) + inLoop(amount - list.head, list)
    }
    inLoop(money, coins)
  }
}
