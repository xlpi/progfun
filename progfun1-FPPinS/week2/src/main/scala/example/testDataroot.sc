class A
case class B extends A
case class C extends A
case class D extends A


object A {
  def apply(v1: Int, v2 Int, v3: Int): A = {
    if(v1%v2 == 0) B
    else if(v2%v3 ==0) C
    else D
  }
}


class Z extends B with C with D {

}


// use
new Z().apply(8375, 237, 3) match {
  case B => print("b")
  case C => print("c")
  case D => print("d")
}