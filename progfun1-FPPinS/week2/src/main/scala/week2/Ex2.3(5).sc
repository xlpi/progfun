import math.abs

object exercise {

  val tolerance = 0.0001

  def isCloseEnough(x: Double, y: Double) =
  abs((x - y) / y) / x < tolerance

  def fixedPoint(f: Double => Double)(firstGuess: Double) = {
    def iterate(guess: Double): Double = {
      println("guess = " + guess +
      " f = " + f(guess))

      val next = f(guess) //1.75

      if(isCloseEnough(guess, next)) next
      else iterate(next)
    }
    iterate(firstGuess)
  }

  fixedPoint(x =>1 + x/2.0)(1.5)

//  def sqrt(x: Double) =
//    // fixedPoint(y => x / y)(1.5)      -- infinite
//    fixedPoint(y => ((y + x / y) / 2))(1.5)  // use avarage Damp

  def averageDamp(f: Double => Double)(x: Double) = {
    (x + f(x)) / 2
  }

  def sqrt(x: Double) = {
    fixedPoint(averageDamp(y => x /y))(1.0)
  }
  sqrt(256)
}