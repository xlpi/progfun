package week2

/**
  * Created by xlpi on 4/4/17.
  */
object Exercise2 extends App {

  def sum(f: Int => Int)(a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a + 1, f(a) + acc)
    }
    loop(a, 0)
  }

  val a = 0
  val b = 10
  println("Sum of all number in bounds:\na = " + a + "\nb = " + b + "\nres = " +
    sum(x => x)(0, 4))


}
