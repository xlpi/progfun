package week2

/**
  * Created by xlpi on 4/5/17.
  */
object exercise3sum extends App{

  def fact(n: Int) = {
    // @tailrec
    def loop(c: Int, acc: Int): Int = {
      if (c <= 1) acc
      else loop(c - 1, c * acc)
    }
    loop(n, 1)
  }

  def sum(f: Int => Int): (Int, Int) => Int = {
    def sumF(a: Int, b: Int): Int = {
      if(a > b) 0
      else f(a) + sumF(a + 1, b)
    }
    sumF
  }

  def cube = (x: Int) => x * x * x


  def sumInts = sum( x => x)
  def sumCubes = sum( x=> x * x * x)
  def sumFactorials = sum(fact)

  val res = sumCubes(1, 10) + sumFactorials(10,20)
  println("sums + facts = " + res)

  //or
  val res2 = sum(cube)(3, 5)

  // sumCubes == sum(cube)

 println("sum(cube)(3,5) = " + res2)

  //or
  val res3 = (sum(cube))(3,5)

  def sumCurr2(f: Int => Int)(a: Int, b: Int): Int = {
    if (a > b) 0 else f(a) + sumCurr2(f)(a + 1, b)
  }

  def res4 = sumCurr2(cube) _
  val res44 = res4(3,5)
  println("sumCurr2(cube) than apply (3,5) == " + res44)

  val res5 = sumCurr2(x=>x)(1, 4)
  println("sumCurr2 = " + res5)




}
