package week2

/**
  * Created by xlpi on 4/5/17.
  */
object exercise4Product extends App {

  def product(f: Int => Int)(a: Int, b: Int): Int = {
   // if (a > b) 1 else f(a) * product(f)(a + 1, b)
    mapReduce(f,(x,y) => x * y, 1)(a, b)
  }

  println("*product(x => x)(3,5) = " +
    product(x => x)(3, 5) + " with mapReduce()")
  println("*product(x=> x* x)(3,5) = "
    + product(x => x * x)(3, 50) + " with mapReduce()")


  def factCurr(n: Int) = product(x => x)(1, n)

  println("def factCurr = product(x => x)(1, n); factCurr(5) = " +
    factCurr(5))

  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int =
    if (a > b) zero
    else combine(f(a), mapReduce(f, combine, zero)(a + 1, b))

  }


