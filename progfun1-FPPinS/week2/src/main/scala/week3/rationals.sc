object rationals {

  // Less 2.5, 2.6

  class Rational(x: Int, y: Int) {
    require(y != 0, "denominator must be nonzero")

    def this(x: Int) = this(x, 1) // second Constructor with persistance y = 1
    private def gcd(a: Int, b: Int): Int = {
      if (b == 0) a
      else gcd(b, a % b)
    }

    private val g = gcd(x, y)

    def numer = x / g
    def denom = y / g

    def + (that: Rational) = new Rational(
      numer * that.denom + that.numer * denom,
      denom * that.denom)

    def unary_- : Rational = new Rational(-numer, denom)

    def - (that: Rational) = this + -that

    def < (that: Rational): Boolean = {
      numer * that.denom < that.numer * denom
    }

    def max(that: Rational): Rational =
      if(this < that) that else this

    override def toString = {
      numer + "/" + denom
    }

  }

  val x0 = new Rational(1, 2)
  val numer1 = x0.numer
  val denom1 = x0.denom
  val y0 = new Rational(2, 3)

  val res2 = x0 + y0

  // Ex 2.5.1
  val x = new Rational(1, 3)
  val y = new Rational(5, 7)
  val z = new Rational(3, 2)

  val res251 = x + y

  val resNeg = -res251

  val xMinusY = x - y

  val xMinusYminusZ = x - y - z
  val yPlusY = y + y

  //val re = 40 % 12003
  val qXlessY = x < y

  val xMaxY = x max y

//  val strange = new Rational(1,0)
//  val sPlusS = strange.add(strange)
  val solidRational = new Rational(3)

  -x

  import week4._

  List(1, 2)



}


