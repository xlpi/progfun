package week3

/**
  * Created by xlpi on 4/9/17.
  */
object rationalClass extends App{

  // Less 2.5

  class Rational(x: Int, y: Int) {
    require(y != 0, "denominator must be nonzero")

    private def gcd(a: Int, b: Int): Int = {
      if (b == 0) a
      else gcd(b, a % b)
    }

    private val g = gcd(x, y)

    def numer = x / g
    def denom = y / g

    def add(that: Rational) = new Rational(
      numer * that.denom + that.numer * denom,
      denom * that.denom)

    def neg = new Rational(-numer, denom)

    def sub(that: Rational): Rational = add(that.neg)

    def less(that: Rational): Boolean = {
      this.numer * that.denom < that.numer * this.denom
    }

    def max(that: Rational): Rational =
      if(this.less(that)) that else this

    override def toString = numer + "/" + denom
  }

  val x0 = new Rational(70, 49)
  val numer1 = x0.numer
  val denom1 = x0.denom
  val y0 = new Rational(2, 3)

  val res2 = x0.add(y0)

  // Ex 2.5.1
  val x = new Rational(1, 3)
  val y = new Rational(5, 7)
  val z = new Rational(3, 2)

  val res251 = x.add(y)

  val resNeg = res251.neg

  val xMinusY = x.sub(y)

  val xMinusYminusZ = x.sub(y).sub(z)
  val yPlusY = y.add(y)

  //val re = 40 % 12003

//  val strange = new Rational(1,0)
//  val sPlusS = strange.add(strange); println("0 - in denom " + sPlusS)

  //assert(-1 > 0)
}
