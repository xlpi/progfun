package recfun

import scala.annotation.tailrec

object Main {

  def main(args: Array[String]) {

    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  println("Balance function")
  val s1 = "(if (zero? x) max (/ 1 x))".toList
  val s2 = "I told him (that it’s not (yet) done). (But he wasn’t listening)".toList
  val s3 = ":-)".toList
  val s4 = "())(".toList // true TODO fix

  println("For s1: \"" + s1 + "\" - \nResult s1: " + balance(s1))

  println("For s1: \"" + s2 + "\" - " + balance(s2))
  println("For s1: \"" + s3 + "\" - " + balance(s3))
  println("For s1: \"" + s4 + "\" - " + balance(s4))

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || r == 0) 1
    else if (r == 1) c
    else {
      fact(r) / (fact(c) * fact(abs(r - c)))
    }
  }

  def fact(n: Int) = {
    @tailrec
    def loop(c: Int, acc: Int): Int = {
      if (c <= 1) acc
      else loop(c - 1, c * acc)
    }
    loop(n, 1)
  }

  def abs(x: Int): Int = if (x < 0) -x else x

  /**
    * Exercise 2
    * Write a recursive function which verifies the balancing of parentheses in a string, which we represent as a List[Char] not a String. For example, the function should return true for the following strings:
    * *
    * (if (zero? x) max (/ 1 x))
    * I told him (that it’s not (yet) done). (But he wasn’t listening)
    * The function should return false for the following strings:
    * *
    * :-)
    * ())(
    * The last example shows that it’s not enough to verify that a string contains the same number of opening and closing parentheses.
    * *
    * Do this exercise by implementing the balance function in Main.scala. Its signature is as follows:
    * *
    *
    *
    * 1
    * def balance(chars: List[Char]): Boolean
    * There are three methods on List[Char] that are useful for this exercise:
    * *
    *chars.isEmpty: Boolean returns whether a list is empty
    *chars.head: Char returns the first element of the list
    *chars.tail: List[Char] returns the list without the first element
    * Hint: you can define an inner function if you need to pass extra parameters to your function.
    * *
    * Testing: You can use the toList method to convert from a String to aList[Char]: e.g. "(just an) example".toList.
    */
  def balance(chars: List[Char]): Boolean = {

    def closeO(myList: List[String]): List[String] = {

      val index = myList.indexOf("o")
      if (index == -1) myList
      else myList.updated(index, "c")
    }

    {
      def balanceCount(tail: List[Char]): Boolean = {

        def inLoop(tail: List[Char], acc: List[String]): List[String] = {
            
          if (tail.length == 0) acc
          else inLoop(tail.tail, {
            if (tail.head == '(') acc :+ "o"
            else if (tail.head == ')') {
              if (acc.contains("o")) closeO(acc)
              else acc :+ "o"
            } else acc
          })
        }

        if (!(inLoop(tail, List[String]()).contains("o"))) true else false
      }

      /* var1
    def takeIfDefined(inList: List[Boolean]): Boolean = {
      println(Console.RED +"takeIfDefined will return inList.isDefinedAt(inList.length): " + Console.RESET + {inList.isDefinedAt(inList.length)} + "\n" + Console.RED + "list: " +
        Console.RESET )
      inList.foreach(println _)
      if(inList.isDefinedAt(inList.length)) inList.last else true
    }

    def currentBoolean(currentItem: Char,prevList: List[Boolean]): List[Boolean] = {
      // insert in if - condition of current char
      // if curr == ')'&& (prevList == false && prevList!isEmpty) => prevList :+ true
      // if curr == '(' => prevList :+ false
      // else => prevList
      if(currentItem == ')' && (takeIfDefined(prevList) == false) ) {
        prevList :+ true
      }
      else if(currentItem == '(') {
        prevList :+ false
      }
      else prevList

      // ret empty - prevList,
    }

    def foldBoolean(list: List[Boolean]): Boolean = {
      def inLoop(tailList: List[Boolean],acc: Boolean): Boolean = {
        if(tailList.length == 0) acc
        else inLoop(tailList.tail, {if(tailList.head == true) true else false })
      }
      inLoop(list, false)
    }

    def balanceCount(tail: List[Char], list: List[Boolean]): Boolean = {

    }
    */

      if (chars.isEmpty) throw (new NoSuchElementException)
      else balanceCount(chars)
    }

    /*   var 0
     if (chars.isEmpty) throw (new NoSuchElementException)
    else {
      if ({
        @tailrec
        def balanceCount(tail: List[Char], acc: Int): Int =
          if (tail.length == 0) acc
          else balanceCount(tail.tail, acc + {
            if (tail.head == '(') -1 else if (tail.head == ')') 1 else 0
          })
        balanceCount(chars, 0)
      } == 0) true
      else false
    }
  }
*/

  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {
    ???
  }

}

