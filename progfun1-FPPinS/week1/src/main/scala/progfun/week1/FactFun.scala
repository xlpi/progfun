package progfun.week1

import scala.annotation.tailrec

/**
  * Created by xlpi on 3/31/17.
  */
object FactFun extends App{

  def fact(n: BigInt) = {
         // @tailrec
    def loop(c: BigInt, acc: BigInt): BigInt = {
      if (c <= 1) acc
      else loop(c - 1, c * acc)
    }
    loop(n, 1)
  }

  def printL(b: BigInt) = {
    @tailrec
    def printLoop(bound: BigInt, u :Unit): Unit = {
      if (bound > b) ()
      else printLoop(bound + 1, {println("Fact(" + bound + ") = " + fact(bound))})
    }
    printLoop(0, ())
  }

  //printL(2000)
  val number = 900000
  println("Fact(" + number + ") = " + fact(number)) //.apply(900000) - ok

}
