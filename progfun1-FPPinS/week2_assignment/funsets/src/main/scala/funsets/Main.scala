package funsets

object Main extends App {
  import FunSets._
  println(contains(singletonSet(1), 1))

  val s1 = Set(1,2,3,4,5,6,7,8,9,10)
import FunSets._
  val s2 = s1.map( _ * 500)
  println(s2)
  printSet(s2)
}
